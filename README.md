# Camerang

See and snap pictures of the camera of one device from another one.
No setup required thanks to GameKit. Launch the app on the device
you want to use as a camera first, any additional instance of the
app will instantly show its camera view in real time. Tap to snap
a picture. Tap to switch between front and back camera on the host
device. Works on iPhone, iPad and Mac.

