//
//  main.m
//  Camerang
//
//  Created by Boris Bügling on 07.05.13.
//  Copyright (c) 2013 Boris Bügling. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BBUAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BBUAppDelegate class]));
    }
}
