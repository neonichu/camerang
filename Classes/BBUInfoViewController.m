//
//  BBUInfoViewController.m
//  Camerang
//
//  Created by Boris Bügling on 08.05.13.
//  Copyright (c) 2013 Boris Bügling. All rights reserved.
//

#import "BBUInfoViewController.h"

@interface BBUInfoViewController ()

@property (nonatomic, strong) NSString* path;

@end

#pragma mark -

@implementation BBUInfoViewController

-(id)initWithResource:(NSString*)resourceName ofType:(NSString*)type {
    self = [super init];
    if (self) {
        self.path = [[NSBundle mainBundle] pathForResource:resourceName ofType:type];
    }
    return self;
}

-(void)loadView {
    self.view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.view.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:128.0/255.0 blue:38.0/255.0 alpha:1.0];
}

-(void)tapped {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:NULL];
}

-(void)viewDidLoad {
    UIWebView* webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    webView.backgroundColor = [UIColor clearColor];
    webView.opaque = NO;
    [webView loadHTMLString:[NSString stringWithContentsOfFile:self.path encoding:NSUTF8StringEncoding error:nil]
                    baseURL:[[NSBundle mainBundle] bundleURL]];
    [self.view addSubview:webView];
    
    for (UIView* subview in webView.scrollView.subviews) {
        if ([NSStringFromClass([subview class]) isEqualToString:@"UIWebBrowserView"]) {
            UITapGestureRecognizer* tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                            action:@selector(tapped)];
            tapRecognizer.numberOfTapsRequired = 2;
            [subview addGestureRecognizer:tapRecognizer];
        }
    }
}

@end
