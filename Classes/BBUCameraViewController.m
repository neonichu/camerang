//
//  BBUCameraViewController.m
//  Camerang
//
//  Created by Boris Bügling on 07.05.13.
//  Copyright (c) 2013 Boris Bügling. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>

#import "BBUCameraViewController.h"
#import "BBUInfoViewController.h"
#import "BOMTalk.h"
#import "RRCameraCaptureSession.h"
#import "WBErrorNoticeView.h"
#import "WBSuccessNoticeView.h"

static NSString* const kFirstLaunch = @"kFirstLaunch";
static const NSUInteger kSendImage  = 101;

@interface BBUCameraViewController () <RRCameraCaptureSessionDelegate>

@property (nonatomic, strong) RRCameraCaptureSession* captureSession;
@property (nonatomic, strong) UIImageView* imageView;
@property (nonatomic, strong) NSDate* lastTime;
@property (nonatomic, readonly) BOMTalk* talker;

@end

#pragma mark -

@implementation BBUCameraViewController

-(void)cameraCaptureSession:(RRCameraCaptureSession *)session capturedImage:(UIImage *)image {
    self.imageView.image = image;
    
    if ([[NSDate new] timeIntervalSinceDate:self.lastTime] > 0.2) {
        self.lastTime = [NSDate new];
        
        for (BOMTalkPeer* peer in self.talker.peerList) {
            [self.talker connectToPeer:peer success:^(BOMTalkPeer *peer){
                [self.talker sendMessage:kSendImage toPeer:peer withData:UIImageJPEGRepresentation(image, 0.6)];
            }];
        }
    }
}

-(id)init {
    self = [super init];
    if (self) {
        self.lastTime = [NSDate new];
        
        [self performBlock:^{
            if (self.talker.peerList.count <= 0) {
                self.captureSession = [RRCameraCaptureSession new];
                self.captureSession.delegate = self;
                [self.captureSession start];
            } else {
                [self.talker answerToMessage:kSendImage block:^(BOMTalkPeer *sender, id data) {
                    self.imageView.image = [UIImage imageWithData:data];
                }];
            }
        } afterDelay:1.0];
        
        [self.talker startInMode:GKSessionModePeer];
    }
    return self;
}

-(void)performBlock:(void(^)(void))block afterDelay:(NSTimeInterval)delay {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_SEC), dispatch_get_main_queue(), block);
}

-(void)swiped {
    BBUInfoViewController* infoVC = [[BBUInfoViewController alloc] initWithResource:@"about" ofType:@"html"];
    [self presentViewController:infoVC animated:YES completion:NULL];
}

-(BOMTalk *)talker {
    return [BOMTalk sharedTalk];
}

-(void)tapped {
    ALAssetsLibrary* assetLibrary = [ALAssetsLibrary new];
    [assetLibrary writeImageToSavedPhotosAlbum:self.imageView.image.CGImage
                                      metadata:nil
                               completionBlock:^(NSURL *assetURL, NSError *error) {
                                   if (assetURL) {
                                       WBSuccessNoticeView* notice = [WBSuccessNoticeView
                                                                      successNoticeInView:self.view
                                                                      title:NSLocalizedString(@"Picture saved to Camera Roll.",
                                                                                              nil)];
                                       [notice show];
                                   } else {
                                       WBErrorNoticeView* notice = [WBErrorNoticeView
                                                                    errorNoticeInView:self.view
                                                                    title:NSLocalizedString(@"Error", nil)
                                                                    message:error.localizedDescription];
                                       [notice show];
                                   }
                               }];
}

-(void)viewDidAppear:(BOOL)animated {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults boolForKey:kFirstLaunch]) {
        BBUInfoViewController* infoVC = [[BBUInfoViewController alloc] initWithResource:@"first-launch" ofType:@"html"];
        [self presentViewController:infoVC animated:YES completion:NULL];
        [defaults setBool:YES forKey:kFirstLaunch];
    }
}

-(void)viewDidLoad {
    self.imageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    self.imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.imageView.backgroundColor = [UIColor blackColor];
    self.imageView.userInteractionEnabled = YES;
    [self.view addSubview:self.imageView];
    
    UISwipeGestureRecognizer* swipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swiped)];
    swipeRecognizer.direction = UISwipeGestureRecognizerDirectionUp;
    [self.imageView addGestureRecognizer:swipeRecognizer];
    
    UITapGestureRecognizer* tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped)];
    [self.imageView addGestureRecognizer:tapRecognizer];
}

@end
