//
//  BBUInfoViewController.h
//  Camerang
//
//  Created by Boris Bügling on 08.05.13.
//  Copyright (c) 2013 Boris Bügling. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BBUInfoViewController : UIViewController

-(id)initWithResource:(NSString*)resourceName ofType:(NSString*)type;

@end
